import { Router } from 'express';

import QuoteController from './controllers/QuoteController';
import HistoricalPriceController from './controllers/HistoricalPriceController';

const routes = Router();

/* Routes for Quotes */
routes.get('/quotes', QuoteController.index);
routes.post('/quotes', QuoteController.store);

/* Routes for Historical Prices (Chart) */
routes.get('/historical-prices/:symbol', HistoricalPriceController.index);
routes.post('/historical-prices/:symbol', HistoricalPriceController.store);

export default routes;
