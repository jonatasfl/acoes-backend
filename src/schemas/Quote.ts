import { Schema, model, Document } from 'mongoose';

const QuoteSchema = new Schema({
  symbol: String,
  companyName: String,
  primaryExchange: String,
  calculationPrice: String,
  open: Number,
  openTime: Number,
  close: Number,
  closeTime: Number,
  high: Number,
  low: Number,
  latestPrice: Number,
  latestSource: String,
  latestTime: String,
  latestUpdate: Number,
  latestVolume: Number,
  iexRealtimePrice: Number,
  iexRealtimeSize: Number,
  iexLastUpdated: Number,
  delayedPrice: Number,
  delayedPriceTime: Number,
  extendedPrice: Number,
  extendedChange: Number,
  extendedChangePercent: Number,
  extendedPriceTime: Number,
  previousClose: Number,
  previousVolume: Number,
  change: Number,
  changePercent: Number,
  volume: Number,
  iexMarketPercent: Number,
  iexVolume: Number,
  avgTotalVolume: Number,
  iexBidPrice: Number,
  iexBidSize: Number,
  iexAskPrice: Number,
  iexAskSize: Number,
  marketCap: Number,
  peRatio: Number,
  week52High: Number,
  week52Low: Number,
  ytdChange: Number,
  lastTradeTime: Number,
  isUSMarketOpen: Boolean,
}, {
  timestamps: true,
});

interface IQuote extends Document {
  symbol: string;
  companyName: string;
  primaryExchange: string;
  calculationPrice: string;
  open: number;
  openTime: number;
  close: number;
  closeTime: number;
  high: number;
  low: number;
  latestPrice: number;
  latestSource: string;
  latestTime: string;
  latestUpdate: number;
  latestVolume: number;
  iexRealtimePrice: number;
  iexRealtimeSize: number;
  iexLastUpdated: number;
  delayedPrice: number;
  delayedPriceTime: number;
  extendedPrice: number;
  extendedChange: number;
  extendedChangePercent: number;
  extendedPriceTime: number;
  previousClose: number;
  previousVolume: number;
  change: number;
  changePercent: number;
  volume: number;
  iexMarketPercent: number;
  iexVolume: number;
  avgTotalVolume: number;
  iexBidPrice: number;
  iexBidSize: number;
  iexAskPrice: number;
  iexAskSize: number;
  marketCap: number;
  peRatio: number;
  week52High: number;
  week52Low: number;
  ytdChange: number;
  lastTradeTime: number;
  isUSMarketOpen: boolean;
}

export default model<IQuote>('Quote', QuoteSchema);
