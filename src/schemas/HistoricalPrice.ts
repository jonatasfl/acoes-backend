import { Schema, model, Document } from 'mongoose';

const HistoricalPriceSchema = new Schema({
  symbol: String,
  date: String,
  high: Number,
  low: Number,
  volume: Number,
  open: Number,
  close: Number,
  uHigh: Number,
  uLow: Number,
  uVolume: Number,
  uOpen: Number,
  uClose: Number,
  changeOverTime: Number,
  label: String,
  change: Number,
  changePercent: Number,
}, {
  timestamps: true,
});

interface IHistoricalPrice extends Document {
  symbol: string;
  date: string;
  high: number;
  low: number;
  volume: number;
  open: number;
  close: number;
  uHigh: number;
  uLow: number;
  uVolume: number;
  uOpen: number;
  uClose: number;
  changeOverTime: number;
  label: string;
  change: number;
  changePercent: number;
}

export default model<IHistoricalPrice>('HistoricalPrice', HistoricalPriceSchema);
