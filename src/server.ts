import app from './app';

app.listen(process.env.SERVER_PORT || 3333, () => console.log('Server is listening...'));
