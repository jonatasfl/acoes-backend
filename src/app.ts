import "dotenv/config";
import express from "express";
import cors from "cors";
import mongoose from "mongoose";

import routes from "./routes";

class App {
  public express: express.Application;

  constructor() {
    this.express = express();
    this.middlewares();
    this.database();
    this.routes();
  }

  private middlewares(): void {
    this.express.use(express.json());
    this.express.use(cors());
  }

  private async database(): Promise<any> {
    try {
      await mongoose.connect(process.env.DB_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
      });
    } catch (e) {
      console.log("Failed to connect to MongoDB", e);
    }
  }

  private routes(): void {
    this.express.use(routes);
  }
}

export default new App().express;
