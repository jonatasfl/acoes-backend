import { Request, Response } from 'express';

import Quote from '../schemas/Quote';

class QuoteController {
  /**
   * Lists all quotes
   * @param {Request} req
   * @param {Response} res
   * @return {Array}
   */
  public async index(req: Request, res: Response): Promise<Response> {
    const quotes = await Quote.find();
    return res.json(quotes);
  }

  /**
   * Store a quote
   * @param {Request} req
   * @param {Response} res
   * @return {Array}
   */
  public async store(req: Request, res: Response): Promise<Response> {
    try {
      const quote = await Quote.create(req.body);
      return res.json(quote);
    } catch (e) {
      return res.status(500).json({ error: 'Error saving the quote' });
    }
  }
}

export default new QuoteController();
