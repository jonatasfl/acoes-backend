import { Request, Response } from 'express';

import HistoricalPrice from '../schemas/HistoricalPrice';

class HistoricalPriceController {
  /**
   * Lists all historical prices by symbol
   * @param {Request} req
   * @param {Response} res
   * @returns {Array}
   */
  public async index(req: Request, res: Response): Promise<Response> {
    const historicalPrices = await HistoricalPrice.find({ symbol: req.params.symbol });
    return res.json(historicalPrices);
  }

  /**
   * Store all historical prices
   * @param {Request} req
   * @param {Response} res
   * @return {Array}
   */
  public async store(req: Request, res: Response): Promise<Response> {
    try {
      const historicalPriceWithSymbol = req.body.map((item) => {
        const newItem = { ...item, symbol: req.params.symbol };
        return newItem;
      });
      const historicalPrices = await HistoricalPrice.insertMany(historicalPriceWithSymbol);
      return res.json(historicalPrices);
    } catch (e) {
      return res.status(500).json({ error: 'Error saving the historical prices' });
    }
  }
}

export default new HistoricalPriceController();
