# Backend (teste)

Backend de um teste para vaga de Engenheiro de software.
Neste teste optei por utilizar as seguintes tecnologias:

- **NodeJs** - Uma das "linguagens" utilizadas pela empresa e que eu gosto de trabalhar.
- **Express.js** - Framework simples, porém poderoso.
- **Typescript** - Para deixar o código mais fácil de ser compreendido por outros desenvolvedores.
- **MongoDB** - Banco de dados NoSQL, também utilizado pela empresa.

## Instalação

#### Local

Antes de rodar a API, você deve configurar a porta desejada e o acesso ao banco de dados. Para isso, basta renomear o arquivo `.env.example` para `.env` e setar os atributos `SERVER_PORT` e `DB_URI`.

Feito isso, basta executar os comandos:

    npm install
    npm run dev

#### Via Docker

Caso prefira utilizar o docker, basta configurar o parâmetro `DB_URI` no arquivo `.env` com a seguinte informação `'mongodb://db:27017/stock_exchange'` (caso prefira, pode mudar o stock_exchange pelo nome que quiser).

Feito isso, executar o seguinte comando:

    docker-compose up -d

## Como acessar

A api pode ser acessada pelo endereço:

    # Caso utilize a instalação local ou pelo Docker normal
    http://localhost:3333

    # Caso utilize o Docker Toolbox (legado)
    http://IP_DO_DOCKER_MACHINE:3333
