FROM node:11-alpine

WORKDIR /node-app

COPY package.json .

RUN npm install --quiet

COPY . .

EXPOSE 3333

CMD ["npm", "run", "dev"]
